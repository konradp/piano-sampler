// This is the main app
window.addEventListener('load', () => {
  let debug = false;
  let midiInitialised = false;
//  let piano = new Piano('/samples');


  let piano = new Piano(window.location.href + 'samples');
  let root = document.querySelector('#ex1');

  // Init MIDI
  document.body.addEventListener('click', () => {
    console.log('init midi'); // DEBUG
    if (!midiInitialised) {
      navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
    }
    midiInitialised = true;
  });


  // MIDI functions
  function onMIDIFailure(msg) {
    console.log("Failed to get MIDI access - " + msg);
  }
  function onMIDIMessage(event) {
    // Event types:
    // - 144: note on
    // - 128: note off
    let eve = event.data[0];
    let key = event.data[1];
    let vel = event.data[2];
    if (debug) {
      console.log('EVENT', eve, key, vel);
    }
    if (eve == 144) {
      // note on
      piano.noteOn(key);
    } else if (eve == 128) {
      // TODO: note off
      piano.noteOff(key);
    }
  }
  function onMIDISuccess(midiAccess) {
    // MIDI has been initialised
    console.log('MIDI initialised');
    midi = midiAccess;
    uiSetMidiDevicesList();
    midiAccess.inputs.forEach((input) => {
      input.onmidimessage = onMIDIMessage;
    });
  }
  function uiSetMidiDevicesList() {
    var midiDevices = document.querySelector('#midi-devices');
    midiDevices.innerHTML = '';
    var i = 0;
    for (var entry of midi.inputs) {
      i++;
      var input = entry[1];
      var option = document.createElement('option');
      //option.value = input.id;
      option.innerHTML = input.name;
      option.value = input.id;
      if (i == midi.inputs.size) {
        option.selected = true;
      }
      midiDevices.appendChild(option);
    }
  }


});
