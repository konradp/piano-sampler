window.addEventListener('load', () => {
  let piano = new Piano('/samples');
  let root = document.querySelector('#ex2');
  btn1 = root.querySelector('#btn1')
    .addEventListener('click', () => {
      piano.play(60);
    });
  btn2 = root.querySelector('#btn2')
    .addEventListener('click', () => {
      piano.play(61);
    });
});
