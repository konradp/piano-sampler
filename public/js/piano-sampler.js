// from ADSR, only R is used to simulate the hammers
midi = null;

class Piano {
  // Load and play sounds
  samples = {};
  gainNodes = {};
  debug = true;
  // The path from which to load samples
  samplesPath = '../samples';
  midiInitialised = false;


  constructor(samplesPath = null) {
    this.initAudioContext();
    if (samplesPath != null) {
      this.samplesPath = samplesPath;
    }
    this.loadSamples();
  } // constructor

  initAudioContext() {
    // Note: needs to be triggered by onClick
    try {
      // Init AudioContext
      this.context = new AudioContext();
      if(!this.context)
        this.context = new webkitAudioContext();
      if(!this.context)
        this.context = new window.webkitAudioContext();
    } catch(e) {
      throw 'Web Audio API is not supported in this browser';
    }
  } // initAudioContext


  loadSamples() {
    let urlSamplesIndex = `${this.samplesPath}/index.json`;
    // Fetch index file
    fetch(urlSamplesIndex)
      .then(response => {
        if (!response.ok) {
          throw new Error('HTTP error:', response.status);
        }
        return response.json();
      })
      .then(sampleList => {
        // Load each sample
        for (let note in sampleList) {
          this.loadSound(note, sampleList[note]);
        }
        console.log('Samples loaded');
      })
      .catch(err => {
        console.log('ERROR: Failed to load samples index.json:', urlSamplesIndex, err);
      })
  } // loadSamples


  noteOn(name) {
    // Play note
    if (!this.context) {
      console.log('Init context');
      this.initAudioContext();
    }
    let audioBuffer = this.samples[name];
    if(!audioBuffer) {
      console.log('Could not play', name);
      return false;
    }
    if (this.debug) console.log('Playing', name);
    let source = this.context.createBufferSource();
    source.buffer = audioBuffer;
    //source.connect(this.context.destination); // speakers

    let gainNode = this.context.createGain();
    source.connect(gainNode);
    gainNode.connect(this.context.destination);
    this.gainNodes[name] = gainNode;
    gainNode.gain.setValueAtTime(1, this.context.currentTime);
    source.start(0);

    // ADSR
    //let releaseTime = 0.1;
    //gainNode.gain.linearRampToValueAtTime(1, this.context.currentTime + attackTime);
  } // play

  noteOff(name) {
    console.log(name);
    console.log(this.gainNodes);
    let gainNode = this.gainNodes[name];
    if (!gainNode) {
      console.log('No note is currently playing');
      return;
    }
    let releaseTime = 0.6; // Ensure this matches your attack/release settings
    //gainNode.gain.linearRampToValueAtTime(0, this.context.currentTime + releaseTime);
    gainNode.gain.setValueAtTime(gainNode.gain.value, this.context.currentTime);
    gainNode.gain.exponentialRampToValueAtTime(0.001, this.context.currentTime + releaseTime);
    setTimeout(() => {
    }, releaseTime * 1000); // Convert to milliseconds
    //// Optionally disconnect the gain node after the release is complete
    //setTimeout(() => {
    //  console.log('offing', name);
    //  console.log(gainNode);
    //  gainNode.disconnect();
    //  gainNode = null; // Clear the reference
    //}, releaseTime * 1000); // Convert to milliseconds
  } // noteOff


  //////////////// PRIVATE ////////////////////
  loadSound(name, fileName) {
    // Load sound
    // Example: load('H', 'sounds/sound1.ogg');
    // Note: fileName is just the filename.extension, not full path
    //       the full path is built from the samplesPath
    fileName = `${this.samplesPath}/${fileName}`;
    var self = this;
    fetch(fileName)
      .then((response => response.arrayBuffer()))
      .then(data => {
        this.context.decodeAudioData(
          data,
          (audio) => { this.samples[name] = audio; },
          (err) => { console.log('Loading failed: ', err); }
        );
        console.log('Loaded', fileName);
      })
  } // loadSound


}; // class Player
